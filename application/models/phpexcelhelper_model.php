<?php
/**
 * Modelo para PHPExcel_helper 
 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
 */

/********************************************************************
 * DOCUMENTACION
 * @doc Estructura y ejemplos de registros de las Tablas PHPExcelH:
 * @in_function PHPExcel_helper 
 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014

 	* Prefijo: phpexcel_ 
* Nombre: phpexcel_layout
* Desc: Tabla que contiene los layouts
	* 	idLayout (Primary Key)
	* 	layoutType (VARCHAR ejemp: indicadores_metas_12) nombre del layout: lleva como nombre de la tabla a la cual inserta datos + un numero identificador
	* 	totalColsExpected (SMALLINT ejemp: 10) columnas esperadas
	* 	totalRowsExpected (SMALLINT ejemp: 1) filas esperadas
	* 	sheetsToRead (VARCHAR ejemp: "hoja1,hoja2") hojas a leer del excel
	* 	range (VARCHAR ejemp: "A1:B16") rango de lectura del excel
	*   kColumns (VARCHAR jemp: "idindicadores,numero_avance,idcat_entidad_medible,idcat_origenes") columnas que pueden actuar como primary key en los update
*
* Nombre: phpexcel_cellsexpected
	* Desc: Tabla que contiene la lista de celdas esperadas para el layout
	* 	idLayout (Llave foranea)
	* 	cell (VARCHAR ejemp: "A1") nombre o posicion de celda
	* 	rule (VARCHAR ejemp: "s|Enero") regla
	* NOTA: Con lo anterior indicamos que la celda 'A1' es un dato tipo 'string = s' y tiene que decir 'Enero' el pipe '|' funciona como separador
*
* Nombre: phpexcel_insertsmap
	* Desc: Tabla que contiene el mapeo para los arreglos con los datos del archivo de excel
	* 	que seran insertados en la base de datos
	* 	idLayout (Llave foranea)	
	*  key (VARCHAR 25 ejemp: 'numero_avance') llave: nombre de columna a la cual se inserta la celda obtenida
	*  dataMap (VARCHAR 25 ejemp: 'nColumna') mapeo de datos: clasificacion respecto a una localizacion de la celda
	* NOTA: Con lo anterior le estamos indicando al mapeo: que en la columna 'numero_avance' de la tabla se va a insertar 'nColumna' (numero de columna del archivo de excel de la que se obtuvo el dato)
 *
 *
 ********************************************************************/
class PHPExcelHelper_model extends CI_Model {
	// Prefijo para las tablas del helper PHPExcel
	private $PHPExcelHPrefix = "phpexcel_";

	// Constructor
	function __construct()
    {
        parent::__construct();
    }
	
    /**
     * Obtiene las celdas y reglas definidas para un layout (tabla: PREFIX_cellsexpected)
     * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
     * @param   Int $idLayout LLave primaria del layout
     * @return  ArrayAssoc o null cuando falla
     */
	function get_cellsExpected($idLayout)	
	{
		$this->db->where('idLayout',$idLayout);
		$result = $this->db->get($this->PHPExcelHPrefix.'cellsexpected');
		if($result->num_rows() > 0){
		// Convierto el resultado e un arreglo asociativo para ser utilizado por el helper
			$insertsMap = $this->result_to_array_assoc($result, 'cell', 'rule');
			return $insertsMap; 
		}
		return null;
	}

	/**
	 * Obtiene un layout con sus configuraciones desde la BD (tabla: PREFIX_layouts)
	 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
	 * @param   String $layoutType nombre del layout del cual se quiere obtener la configuracion del layout
	 * @return  row_array o null cuando falla
	 */
	function get_layout($layoutType)	
	{
		$this->db->where('layoutType', $layoutType);
		$result = $this->db->get($this->PHPExcelHPrefix.'layouts');
		if($result->num_rows() > 0){
			// Convierto el resultado en un arreglo
			$result = (array) $result->row_array();
			return $result; 
		}
		return null;
	}

	/**
	 * Inserta un arreglo de layout en X tabla
	 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
	 * @param   Array $insertArray
	 * @param   String $tableName nombre de la tabla en la cual se insertaran los datos
	 * @param 	Arrray Assoc $insertConfig arreglo con configuraciones para la inserccion de registros en la tabla
	 * @return  Boolean 
	 */
	function insert_layout_to_db($tableName, $insertArray, $insertConfig)
	{
		/********************************************************************
		 * DOCUMENTACION
		 * @doc  Definicion del configurador $insertConfig 
		 * @in_function insert_layout_to_db
		 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
		 *
		 * Tiene que ser definido en la tabla phpexcel_layout por medio del campo kColumn (si no se usa, tiene que ser null)
		 * $insertConfig es una variable conf que define si los registros van a ser insertados y actualizados en caso de ya existir en la tabla
		 * Array $insertConfig['kColumns'] nos indica la lista de columnas a comparar para generar los where (son las columnas que actuan como llave primaria en caso de no poder proporcionarla)
		 ********************************************************************/
		if (!isset($insertConfig['kColumns'])){
			echo "No existe \$insertConfig['kColumns'], defina kColumns en la tabla phpexcel_layouts, si no se usa = null";
			return FALSE;
		}

	  // Tengo un insert con update preconfigurado ?
	  	if (!is_null($insertConfig['kColumns'])) {
	  		
	  		 $this->db->trans_start();

		 	 // Recorro cada uno de los insert como filas
		 	foreach ($insertArray as $insert => $row) {
		 		$flag = TRUE; // bandera para los where concatenados

		 		// Inicializo la query
		 		$queryString = "SELECT * FROM ".$tableName;

		 		// Genero los where
			 	foreach ($insertConfig['kColumns'] as $thisColum) {
			 		if($flag){
			 			$queryString.= " WHERE ".$thisColum." = '".$row[$thisColum]."'";
			 			$flag = FALSE;
			 		}else{
			 			$queryString.= " AND ".$thisColum." = '".$row[$thisColum]."'";
			 		}
			 	}

			 		// Ejecuto la query para obtener las coincidencias de registros
	 		 		$thisQuery = $this->db->query($queryString.";");

	 		 		// Hay mas de un registro que cumple los where (No es posible actualizar dos registros bajo el mismo criterio de columnas primarias definidas)
	 		 		if ($thisQuery->num_rows() > 1) {
	 		 			echo "Error: \$insertConfig['kColumns'] ".lang("columnas_primarias_error");
	 		 			$this->db->trans_rollback();
	 		 			return FALSE;
	 		 		}

				 	 $this->db->from($tableName);

	 		 		// Hay un registro para actualizar
				    if ($thisQuery->num_rows() ===  1) {
				 	  	// Genero los where para el update
					 	foreach ($insertConfig['kColumns'] as $thisColum) {
					    	$this->db->where($thisColum, $row[$thisColum]);
					 	}

				      // La fila existe y actualizo
				      $query = $this->db->update($tableName, $row);
				    } else {
				      //  La fila no existe e inserto
				      $query = $this->db->insert($tableName, $row);
				    }
		 	}

		 	// Verifico transaction
		 	$this->db->trans_complete();
		 	if ($this->db->trans_status() === FALSE)
			{
				echo lang('error_transaccion');
				$this->db->trans_rollback();
			}else{
		   		 $this->db->trans_commit();
				return TRUE;
			}
			 return FALSE;
	  	}else{
	  			// Realizo un insert batch (los registros no se actualizan, se insertan y se pueden duplicar)
				return $this->db->insert_batch($tableName, $insertArray);
	  	}
	}

	/**
	 * Actualiza un arreglo de layout en X tabla
	 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
	 * @param   Array $updateArray
	 * @return  Boolean 
	 */
	function update_layout_to_db($tableName, $updateArray )
	{
		return $this->db->update_batch($tableName, $updateArray);
	}

	/**
	 * Obtiene el insertsMap (Mapeo de inserts), el insertMaps sirve para indicarle al PHPExcelHelper
	 * como distribuir los datos obtenidos del archivo de excel en el arreglo que sera insertado en la base de datos
	 * de esta manera podemos configurar muchos layouts distintos e indicarle en que tablas van los datos y a que campos corresponden
	 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
	 * @param   Int $idLayout id del layout utilizado para los inserts
	 * @return  ArrayAssoc $insertMaps o null en caso de fallar
	 */
	function get_insertsmap($idLayout)
	{
		$this->db->where('idLayout',$idLayout);
		$result = $this->db->get($this->PHPExcelHPrefix.'insertsmap');
		if($result->num_rows() > 0){
		// Convierto el resultado e un arreglo asociativo para ser utilizado por el helper
			$insertsMap = $this->result_to_array_assoc($result, 'key', 'dataMap');
			return $insertsMap; 
		}
		return null;
	}	

	
	/**
	 * Crea un array asociativo a partir de un result 
	 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
	 * @access  privado
	 * @param   String $result resultado de la query
	 * @param   String $keyname nombre del campo de la tabla que sera considerado como llave en el arreglo
	 * @param   String $value nombre del campo de la tabla que sera considerado como valor
	 * @return  Array $array asociativo o null en caso de fallar
	 */
	private function result_to_array_assoc($result, $keyName, $valueName)
	{
		/// Convierto arreglo asociativo
		if ($result->num_rows() > 0)
		{
			foreach ($result->result_array() as $row)
			{
			  $array[$row[$keyName]] = $row[$valueName];
			}
			return $array; 
		}else{
			return null;
		} 
	}

	/**
	 * Obtengo todos los registros de X tabla
	 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
	 * @param   $tableName nombre de la tabla
	 * @return  $result
	 */
	function get_all_from_table($tableName)
	{	
		$result = $this->db->get($tableName);
		if($result->num_rows() > 0){
			return $insertsMap; 
		}
		return null;
	}
}