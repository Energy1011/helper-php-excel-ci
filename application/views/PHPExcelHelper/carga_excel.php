<!-- vista del modal para cargar archivos excel -->
	<!-- TODO: actualizar esto a source local -->
<script type='text/javascript'>
var url_procesa_archivo = '<?=$url_procesa_archivo?>';
var cargando_archivo = "<?=lang('cargando_archivo')?>";

	/**
	 * Funcion que captura el resultado del uploadfile 
	 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
	 */
	function resultadoUpload(result)
	{
		$('#div_mensajes').html(cargando_archivo);
		eval('var jsonObj = '+result+';');
		var resultado = jsonObj.resultado;
		var mensaje = '';

			// Falla al subir el archivo
			if (resultado == 1){
				 mensaje = jsonObj.mensaje;
				 $("#save_import_button").attr('disabled', true);
			}

			// Archivo subido correctamente
			if(resultado == 0) 
			{
				mensaje = jsonObj.mensaje;
				filename = jsonObj.filename;
				// Proceso el archivo excel
				procesar_excel(filename);
			}

			 // Agrego el mensaje al div
			 if (mensaje!='') { $("#div_mensajes").html(mensaje); };

	}

	/**
	 * Funcion con Ajax para procesar un archivo excel importado 
	 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
	 */
	function procesar_excel(filename)
	{
		if ($('#num_layout').length) {
			var num = $('#num_layout').val();
		}else{
			var num = $('#frecuencia_medicion').val();
		}
		//Obtengo el tipo de medicion que hace la diferencia entre layouts
		// Obtengo el tipo de layout y lo concateno al numero de layout correspondiente
		var layoutType = $('#layoutType').val()+'_'+num;
		$('#div_mensajes').html(cargando_archivo);
		$.ajax({  
			type: 'POST',
			dataType: 'json',
			url: url_procesa_archivo,
			data: {filename: filename, layoutType: layoutType}
		}).done(function(json){
			var resultado = json.resultado;
			var mensaje = json.mensaje;
			var html = json.Htmltable;	

			// Layout incompatible
			if (resultado == 1){
				 $("#save_import_button").attr('disabled', true);
				 if (mensaje!='') { $("#div_mensajes").html(mensaje); };
				 $('#tabla_archivo_excel').html(html); 
			}

			// Archivo subido correctamente
			if(resultado == 0) 
			{
				$('#tabla_archivo_excel').html(html); 
				if (mensaje!='') { $("#div_mensajes").html(mensaje); };
				// Activo el boton para guardar el layout
				$("#save_import_button").removeAttr("disabled");    
			}

		}).fail(function(json){
     		$('#div_mensajes').html(json.mensaje); 
			alert(json.mensaje);
			// Desactivo el boton para guardar el layout
		    $("#save_import_button").attr('disabled', true);
		});
		return;	
	}
</script>
<div>
<h2><?=lang('cargar_archivo_excel')?></h2>
</div>

	<!-- Carga el archivo de excel -->
	<h4><?=lang('seleccione_archivo')?></h4>
 	 <?php if (isset($error)){
	 	echo $error;
	 }?>

 <!-- File chooser para subir el archivo de Excel -->
	<?php echo form_open_multipart($url_sube_archivo, array('id'=>'form_upload_file', 'target'=>'myiframe'));?>
	<input type="file" name="userfile" size="20" />
	<input type="submit" value="<?=lang('cargar_archivo')?>" />
	</form>
	
 	<div id='div_mensajes'></div>
	<!-- div para cargar el archivo de excel -->
	<div id='tabla_archivo_excel' style='overflow:scroll;'></div>
	<!-- iframe para evitar cargar y pasar a otra pagina dentro del modal al hacer submit multipat file-->
	<iframe id="myiframe" name="myiframe" hidden></iframe>