<?php
/**
 * Clase helper: Clase controlador para manejar la carga de archivos desde Excel como helper de Codeigniter
 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
 */

class PHPExcelHelper extends CI_Controller {

	// Constructor de la clase
	function __construct()
	{
		parent::__construct();
		// Cargo los helpers para la clase
		$this->load->helper(array('form', 'url', 'language', 'PHPExcel'));
		//Cargo el idioma para el helper
		$this->lang->load('PHPExcelHelper');
		//Cargo el modelo de datos para el helper
		$this->load->model('PHPExcelHelper_model');
		//Cargo sesiones para para guardar el nombre del ultimo archivo subido por el usuario
		$this->load->library('session');
	}

	// Index principal
	public function index()
	{
		$data['url_sube_archivo'] = site_url('PHPExcelHelper/do_upload');
		$data['url_procesa_archivo'] = site_url('PHPExcelHelper/process_excel_file');
		// Cargo la vista 
		$this->load->view('PHPExcelHelper/carga_excel',$data);
	}

	/**
	 * Sube archivos excel al servidor  
	 * LLamada desde Ajax
	 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
	 * @uses    do_upload (libreria upload)
	 * @return  JSON resultado (Resultado de subir el archivo) , mensaje (Mensajes de error o de ok), filename (Nombre del archivo subido)
	 */
	function do_upload()
	{
		$config['upload_path'] = $this->config->item('ruta_upload_files_excel');
		$config['allowed_types'] = 'xls|xlsx';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$this->load->library('upload', $config);
		$resultado = array('resultado' => 0 );
		if ( ! $this->upload->do_upload())
		{	
  			echo '<script type="text/javascript">
					var texto = "'.lang('error_subir_archivo').'";
					alert(texto);
				  </script>';
			$data = array('error' => $this->upload->display_errors(), 'url_procesa_archivo' => site_url('PHPExcelHelper/process_excel_file'), 'url_sube_archivo' => site_url('PHPExcelHelper/do_upload'));
			// Borramos posible archivo subido a la sesion anteriormente
			$this->session->unset_userdata('PHPExcelHelper_last_file_upload');
			$resultado['mensaje'] = $this->upload->display_errors();
			$resultado['resultado'] = 1;
		}
		else
		{
			$data = array('upload_data' => $this->upload->data(), 'url_procesa_archivo' => site_url('PHPExcelHelper/process_excel_file'), 'url_sube_archivo' => site_url('PHPExcelHelper/do_upload'));
			$resultado['resultado'] = 0;
			$resultado['mensaje'] = lang('archivo_cargado_ok');
			$resultado['filename'] = $data['upload_data']['file_name'];
			// Guardamos en la sesion el ultimo archivo subido
			$this->session->set_userdata('PHPExcelHelper_last_file_upload', $data['upload_data']['file_name']);
		}

		//Como es el iframe el que da la respuesta, se utiliza un parent.function();
		echo "<script type='text/javascript'>parent.resultadoUpload('".json_encode($resultado)."');</script>";
	}

	/**
	 * Procesa un archivo de excel importado (verifica el layout y regresa HTML) 
	 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
	 * @uses    PHPEH_excel_to_objPHPExcel para crear el objeto PHPExcel
	 * @uses    PHPEH_objPHPExcel_to_HTML para obtener la tabla HTML del objeto PHPExcel
	 * @param   POST String $inputFileName nombre del archivo importado a procesar
	 * @param   POST String $layputType tipo de layout a utilizar (catalogo BD: PHPExcelH_layout) 
	 * @return  String HTML Codigo html de la tabla
	 */
	function process_excel_file()
	{
		$layoutType = $this->input->post('layoutType');
		$inputFileName = $this->input->post('filename');
		// Compruebo que los post no vengan vacios
		if ($layoutType == '' || $inputFileName == ''){
				//No se ha proporcionado layouttype o filename
				$resultado['mensaje'] = lang('verifique_los_post')." layouttype, inputfilename";
				$resultado['resultado'] = 1;
				echo json_encode($resultado);
				return;
		}

		$data = array('url_procesa_archivo' => site_url('PHPExcelHelper/process_excel_file'));

	// Obtengo la configuracion del layout
		$layoutConfig = $this->PHPExcelHelper_model->get_layout($layoutType);
		// Compruebo que el layoutType exista la configuracion en el catalogo de layouts de la BD
		if (is_null($layoutConfig) || !isset($layoutConfig['idLayout'])) {
			$resultado['mensaje'] = lang('no_existe_layout_en_catalogo').": ".$layoutType;
			$resultado['resultado'] = 1;
			echo json_encode($resultado);
			return;	
		}

		//TODO: hacer cellsExpected para cada hoja del libro del excel ejemplo: $cellsExpected['hoja1'] , $cellsExpected['hoja2']
		// Obtengo las celdas esperadas en caso de existir desde la base de datos	
		$cellsExpected = $this->PHPExcelHelper_model->get_cellsExpected($layoutConfig['idLayout']);
		
		// Agrego las cellsExpected en caso de existir al arreglo de configuracion para el layout
		if (!is_null($cellsExpected)) {
			$layoutConfig['cellsExpected'] = $cellsExpected; // (Array)
		}else{
			// No tenemos reglas para este layout
			$layoutConfig['cellsExpected'] = null;
		}

	 /********************************************************************
		 * DOCUMENTACION
		 * @doc  EJEMPLO DE CONFIGURACION MANUAL PARA UN LAYOUT (poco practico: es mejor traer la config desde la base de datos pero sirve de ejemplo de como estructurar el cellsExpected en la base)
		 * @in_function process_excel_file()
		 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
		 *
		 * FORMATO: <tipo>|<valor>
		 * tipos: n = numero , s = string, * = comodin (cualquier tipo, cualquier valor) etc..
		 * $cellsExpected = array(
		 * 						'C2'=>'s|Enero',
		 * 						'D2'=>'s|Febrero',
		 * 						'E2'=>'s|Marzo',
		 * 						'F2'=>'s|Abril',
		 * 						'G2'=>'s|Mayo',
		 * 						'H2'=>'s|Junio',
		 * 						'I2'=>'s|Julio',
		 * 						'J2'=>'s|Agosto',
		 * 						'K2'=>'s|Septiembre',
		 * 						'L2'=>'s|Octubre',
		 * 						'M2'=>'s|Noviembre',
		 * 						'N2'=>'s|Diciembre',
		 * 						'F3'=>'*|*'); // algo pero no vacia que es igual a '|'
		 * $sheetsToRead = array( 'hoja1'); // lista de hojas a leer null = todas
		 * Defino el layout con totalRows, totalCols y las celdas de encabezado esperadas
		 * $totalColsExpected = 14; // total de columnas esparadas para el layout
		 * $totalRowsExpected = 20; // total de filas esperadas para el layout
		 * $layoutConfig['sheetsToRead'] = "hoja1"; // Lista de hojas para leer, (la lista de hojas no son case sensitive, hice un strtolower() para evitar errores) 
		 * $layoutConfig['range'] = 'B2:N9'; // Defino el rango de lectura para los inserts (contando con los encabezados de columna y de fila)
		 *
		********************************************************************/

		// Defino configuraciones adicionales manualmente (propias del estilo del sistema) 
			// Mostrar mensajes en el html (alertas sobre el layout cuando no es compatible)
			$layoutConfig['show_messages'] = true;
			// Defino el css para la tabla
			$layoutConfig['css_class'] =  "table preview";
			// Defino la cantidad de numeros decimales que se permiten en las cantidades
			//(Energy1011) TODO: Pasar los config para los formatos de numero ?
			$layoutConfig['decimals'] = 2;  // Cantidad de decimales despues del punto o coma
			$layoutConfig['dec_point'] = '.'; // Separador para los decimales
			$layoutConfig['thousands_sep'] = ','; // Separador para los millones

	// Completamos la ruta para el archivo 
		$inputFileName = $this->config->item('ruta_upload_files_excel').$inputFileName;

	// llamamos a la funcion para parsear el archivo de excel
		$objPHPExcel = PHPEH_excel_to_objPHPExcel($inputFileName);
		
		// $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		// var_dump($sheetData);

		if (!isset($objPHPExcel)) {
			//No existe el layout en el catalogo
			$resultado['mensaje'] = lang('error_al_parsear_archivo');
			$resultado['resultado'] = 1;
			echo json_encode($resultado);
			return;
		}
		// Convierto el objPHPExcel a tabla HTML validando las rows y colum esperadas para el layout
			echo PHPEH_objPHPExcel_to_HTML($objPHPExcel, $layoutConfig);

	}

	/**
	 * Inserta en la base de datos un excel importado
	 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
	 * @uses 	session->userdata() de $this->load->library('session') para obtener el ultimo archivo subido al servidor por el usuario
	 * @uses 	PHPExcelHelper_model->get_layout() obtiene la configuracion del layout
	 * @uses 	PHPExcelHelper_model->get_insertsmap() obtiene el mapeo de insertions para la tabla
	 * @uses 	PHPEH_layoutfile_to_array() procesa un layout importado y regresa como arreglo
	 * @uses 	PHPExcelHelper_model->insert_layout_to_db() inserta el arreglo de datos del layout en la base
	 * @param   POST String $layoutType Tipo de layout a procesar (catalogo: phpexcel_layouts)
	 * @param   POST String $tablename Nombre de la tabla donde se inserta el layout
	 * @param   POST Int $idindicadores 
	 * @return  JSON $resultado
	 */
	function insert_imported_file(){
		// Inicializo el arreglo para la respuesta ajax
		$resultado = array();
		$resultado['mensaje'] = '';

		// Obtengo el post del tipo de layout a aplicar
		$layoutType = $this->input->post('layoutType');
		$tableName = $this->input->post('tableName');
		$idindicadores = $this->input->post('idindicadores');

		// Compruebo que los post no vengan vacios
		if ($layoutType == '' || $tableName == '' || $idindicadores == ''){
				//No se ha proporcionado layouttype o filename
				$resultado['mensaje'] = lang('verifique_los_post');
				$resultado['resultado'] = 1;
				echo json_encode($resultado);
				return;
		}

	// Compruebo que exista un tipo definido para el layout 
		if ($layoutType == '') {
			$resultado['mensaje'] = lang('no_hay_layoutType_definido');
			$resultado['resultado'] = 1;
			echo json_encode($resultado);
			return;	
		}

	// Obtengo la ruta y el nombre del archivo importado
		$pathfile = $this->config->item('ruta_upload_files_excel');
		$filename = $pathfile.$this->session->userdata('PHPExcelHelper_last_file_upload');
	
	// Compruebo que el archivo exista
		if (!file_exists($filename)) {
			$resultado['resultado'] = 1;
			$resultado['mensaje'] = lang('archivo_importado_no_encontrado');
			echo json_encode($resultado);
			return;	
		}

	// Obtengo la configuracion del layout
		$layoutConfig = $this->PHPExcelHelper_model->get_layout($layoutType);
		// Compruebo que el layoutType exista la configuracion en el catalogo de layouts de la BD
		if (is_null($layoutConfig)) {
			$resultado['resultado'] = 1;
			$resultado['mensaje'] = lang('no_existe_layout_en_catalogo').": ".$layoutType;
			  echo json_encode($resultado);
			return;	
		}
		//(Energy1011) TODO: paso las config de format_number a la BD ?
			$layoutConfig['decimals'] = 2;  // Cantidad de decimales despues del punto o coma
			$layoutConfig['dec_point'] = '.'; // Separador para los decimales
			$layoutConfig['thousands_sep'] = ','; // Separador para los millones

		// Obtengo el mapeo para el arreglo de los inserts
		$insertsMap = $this->PHPExcelHelper_model->get_insertsmap($layoutConfig['idLayout']);
		
		/********************************************************************
		 * DOCUMENTACION
		 * @doc  Configuracion insert maps variable $insertsMap
		 * @in_function insert_imported_file()
		 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
		 *
		 *  DESCRIPCION: FORMATO EJEMPLO indicadores_avances:
		 * 	$insertsMap['idindicadores'] = 1; // Se obtiene por POST como parametro extra en esta funcion 
		 * 	$insertsMap['numero_avance'] = 'PHPCODE: date('n');';  // Se obtiene el PHPCODE: desde la tabla phpexcel_insertsmap
		 * 	$insertsMap['estado'] = 'encabezadoFila';
		 *  $insertsMap['avance'] = 'datoCuadrante'; 
		 * 	$insertsMap['actualizacion'] = date();
		 * 	$insertsMap['actualizado'] = 'Energy1011';  // Se obtiene el PHPCODE: desde la tabla phpexcel_insertsmap o POST como parametro extra en esta funcion 
		 * 	$insertsMap['idcat_origenes'] = 'encabezadoColumna';
		 *
		 ********************************************************************/
		
		// Existe un insertmap para este layout en la base de datos ?
		if (is_null($insertsMap)) {
			$resultado['resultado'] = 1;
			$resultado['mensaje'] = lang('no_existe_insertsmap');
			  echo json_encode($resultado);
			return;	
		}

//(Energy1011) TODO: Agregar insertMaps extras o adicionales que lleguen desde POST, recorriendolos con un foreach
//  Mapeo adicional para los inserts de manera manual en codigo php
		// NOTA: Esta configuracion no tiene prioridad ante los PHPCODE: de la tabla phpexcel_insertsmap
		// Esto quiere decir que en la funcion siguiente funcion PHPEH_layoutfile_to_array() pueden ser sobre-escritos por un PHPCODE:
			$insertsMap['idindicadores'] = $idindicadores;

	// Obtengo el layout como arreglo para poderlo insertar en la BD 
		$insertArray = PHPEH_layoutfile_to_array($filename, $layoutConfig, $insertsMap);
		if (is_null($insertArray)) {
			$resultado['resultado'] = 1;
			$resultado['mensaje'] = lang('error_crear_arreglo_layout');
			echo json_encode($resultado);
			return;
		}
 		
 		// Preparo el insertConfig
 		if (!is_null($layoutConfig['kColumns'])){	
 			$insertConfig['kColumns'] = explode(',',$layoutConfig['kColumns']);
 		}else{
 			$insertConfig['kColumns'] = null;
 		}

	// Inserto el layout en la base de datos
		if ($this->PHPExcelHelper_model->insert_layout_to_db($tableName, $insertArray, $insertConfig)){
			// Layout insertado correctamente
			$resultado['resultado'] = 0;
			$resultado['mensaje'] = lang('layout_insertado_correctamente');
		}else{
			// Error al insertar el layout en la base de datos
			$resultado['resultado'] = 1;
			$resultado['mensaje'] = lang('error_al_insertar_en_bd');
		}

	// Borro el archivo importado del servidor
			if (file_exists($filename)) {
				unlink($filename);
			}
			
		echo json_encode($resultado);
	}

}// End class
?>