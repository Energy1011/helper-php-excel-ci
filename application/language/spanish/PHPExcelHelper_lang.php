<?php
// Aqui lo que corresponde a Helper PHPExcel_helper
$lang['cargar_archivo'] = 'Cargar Archivo';
$lang['seleccione_archivo'] = 'Seleccione el archivo a cargar';
$lang['el_archivo_no_existe_en_dir'] = 'El archivo no existe en el directorio';
$lang['no_hay_archivo_importado'] = 'No hay archivo importado a procesar.';
$lang['cargando_archivo'] = 'Cargando archivo...';
$lang['error_subir_archivo'] = 'Error al subir el archivo al servidor.';
$lang['error_al_parsear_archivo'] = 'Error al parsear el archivo.';
$lang['total_columnas_sobrepasa'] = 'El número total de columnas sobrepasa las esperadas.';
$lang['total_columnas_menor'] = 'El número total de columnas es menor a las esperadas.';
$lang['total_filas_sobrepasa'] = 'El número total de filas sobrepasa las esparadas.';
$lang['total_filas_menor'] = 'El número total de filas es menor a las esperadas.';
$lang['tipo_dato_incorecto'] = 'El tipo de dato incorrecto';
$lang['se_esperaba'] = 'Se esperaba';
$lang['valor_incorrecto'] = 'Valor incorrecto';
$lang['vacia'] = 'Vacia';
?>