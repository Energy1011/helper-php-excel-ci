<?php 
/**
 *  Helper para codeigniter (importar archivos de excel y verifica layouts)
 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
 */

define('BR','<br />');

/**
 * Funcion que regresa el value de una celda 
 * @param   string cell
 * @param   objPHPExcel $objPHPExcel objeto de libreria PHPExcel
 * @return  string valor (contenido) de la celda
 */
function PHPEH_get_cell($cell, $objPHPExcel){
	//select one cell
	$objCell = ($objPHPExcel->getActiveSheet()->getCell($cell));
	//get cell value
	return $objCell->getvalue();
}

/**
 * pp = plusplus, suma una 'posicion' a un char convertido a ASCII por referencia  
 * Utilizamos esta funcion para sumar letras y avanzar entre columnas
 * @uses    chr()
 * @uses    ord()
 * @param   &char caracter ASCII (por referencia)
 * @return  boolean
 */
function PHPEH_pp(&$var){
	$var = chr(ord($var)+1);
	return true;
}

/**
 * Funcion para convertir un tipo de dato de PHPExcel a modo legible por humanos 
 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
 * @param   char s = String , n = numero , etc
 * @return  string cadena de texto entendible por humanos ejemplo: 'Texto', 'Numero'
 */
function PHPEH_libType_to_humanReadable($typeDataPHPExcel){
// s = string (texto), n = int (numero)
	switch ($typeDataPHPExcel) {
		case 's':
			return lang('texto');
			break;

		case 'n':
			return lang('numero_o_cantidad');
			break;
		
		default:
			return lang('tipo_dato_desconocido').': "'.$typeDataPHPExcel ;
			break;
	}

}

/**
 * Verifica si tiene un rango de lectura definido para el archivo de excel y lo configura 
 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
 * @uses    PHPExcel_Cell::columnIndexFromString() convierte el nombre de la columna a num desde un string
 * @uses    getHighestRow() obtiene la ultima columna del archivo cuando no existe un rango definido
 * @uses    getHighestColumn obtiene la ultima fila del archivo cuando no existe un rango definido
 * @param   Array $layoutConfig configuracion del layout en su campo 'range'
 * @param   PHPExcelObject $worksheet hoja actual del objeto PHPExcel
 * @param   Int &$lowerColumn la primera columna del archivo de la cual se empieza a leer
 * @param   Int &$lowerRow la primera fila del archivo de la cual se empieza a leer
 * @param   Int &$highestColumn la ultima columa del archivo a leer
 * @param   Int &$highestRow la ultima fila del archivo a leer
 * @return  NOT RETURN (modifica variables pasadas por referencia)
 */
function PHPEH_verify_read_range($layoutConfig, $worksheet, &$lowerColumn, &$lowerRow, &$highestColumn, &$highestRow)
{
	// Verifico si tengo un rango de lectura definido
		if (isset($layoutConfig['range']) && !is_null($layoutConfig['range'])) {
			// Divido el string del rango configurado para leer
			$splited = split('[:]',$layoutConfig['range'] );
			$startCell = $splited[0];
			$endCell = $splited[1];

		 //Obtengo la columna (letra) de la primera celda (superior izq) del rango a leer
			preg_match("|[a-zA-Z]|", $startCell, $char);
			$lowerColumn = $char[0];
			// Convierto la columna de formato letra a formato numero
			$lowerColumn = PHPExcel_Cell::columnIndexFromString($lowerColumn);
			$lowerColumn -= 1;
		 
		 //Obtengo el numero de fila de la primera celda (superior izq) del rango a leer
			preg_match("|\d+|", $startCell, $num);
			$lowerRow = $num[0];

			// Limpio el contenido de num y char para reutilizar
			unset($num, $char);

		 //Obtengo la letra de la ultima columna
			preg_match("|[a-zA-Z]|", $endCell, $char);
			$highestColumn = $char[0];

		 //Obtengo el numero de fila de la ultima celda
			preg_match("|\d+|", $endCell, $num);
			$highestRow = $num[0];

		}else{
		    // Obtengo la ultima fila de la hoja actual
		    $highestRow = $worksheet->getHighestRow(); 
		    // Obtengo la ultima columna de la hoja actual
		    $highestColumn = $worksheet->getHighestColumn(); 
		}
}

/**
 * Comprueba si hay una hoja de excel en la lista de hojas a leer  
 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
 * @uses    is_null()
 * @uses    strtolower()
 * @uses    explode()
 * @uses    in_array()
 * @param   String $sheetsToRead lista de hojas permitidas separadas por comas (no case sensitive)
 * @return  Boolean
 */
function PHPEH_is_sheet_in_read_list($sheetsToRead, $thisSheet){
	// Convierto a minusculas los nombres de las hojas para evitar errores con el case-sensitive
	$thisSheet = strtolower($thisSheet);
	$sheetsToRead = strtolower($sheetsToRead);
	// Convierto a arreglo la lista de hojas a leer separadas por comas
	$sheetsArray = explode(',', $sheetsToRead);
	// Si no existe la hoja en la lista de hojas permitidas para leer, continuo a la siguiente iteracion
		if (!in_array($thisSheet, $sheetsArray)) {
			// No existe la hoja en la lista
			return 0;
		}
		return 1;
}

/**
 * Funcion que genera el html desde un objPHPExcel 1.8.0
 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
 * @uses    array_key_exists() Busca un key determinado en el arreglo
 * @uses    PHPEH_is_sheet_in_read_list() Busca una hoja en la lista de hojas permitidas a leer del excel
 * @uses    PHPEH_verify_read_range() Configura y prepara el rango de lectura
 * @uses    PHPEH_libType_to_humanReadable() convierte un tipo de dato de la libPHPExcel a humanreadable
 * @uses    json_encode() Codifica en formato json el resultado para regreso de la funcion
 * @param   objPHPExcel $objPHPExcel Objeto excel
 * @param   Array $layoutConfig configuraciones para el layout (columnas esparadas, rango, mostrar mensajes, estilo css, etc) 
 * @return  String HTML con la tabla o tablas generadas
 */
function PHPEH_objPHPExcel_to_HTML($objPHPExcel, $layoutConfig)
{
	// Obtengo el total de hojas del libro de excel
	$totalsheets = $objPHPExcel->getAllSheets();
	$Htmltable= "";

// Itero sobre las hojas de excel para generar las tablas html
// Con este foreach sobre los iteradores podemos leer varias hojas del excel
	foreach ($totalsheets as $worksheet) {
		// Obtengo el titulo de la hoja
	    $worksheetTitle = $worksheet->getTitle();

	    // La hoja de excel actual existe en la lista de hojas a leer ?
	    if (!is_null($layoutConfig['sheetsToRead']) && !PHPEH_is_sheet_in_read_list($layoutConfig['sheetsToRead'], $worksheetTitle)) {
	    	continue;
	    }

		// Bandera para detectar algun error en el layout
		$flagErrorLayout = false;
		// Variable para acomulado de mensajes de error cuando el layout no empata con el esperado
		$errorsLayout = "";
		
		// Inicializo la primera columna y fila de la cual se empieza a leer el archivo default 0,1 = A1 
		$lowerColumn = 0;
		$lowerRow = 1;
		$highestColumn = 0;
		$highestRow = 0;

	    // Obtengo la ultima fila de la hoja actual
	    $highestRow = $worksheet->getHighestRow(); 
	    // Obtengo la ultima columna de la hoja actual
	    $highestColumn = $worksheet->getHighestColumn(); 

		// Verifico si existe un rango de lectura para el archivo y si existe lo prepara
		// PHPEH_verify_read_range($layoutConfig, $worksheet, $lowerColumn, $lowerRow, $highestColumn, $highestRow);
	    // Convierto el indice (nombre de la columna) a valor numerico 
	    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

	    // Resto - 64 para calcular el valor en ascii de la ultima columna
	    	// 64 es '@' enseguida sigue la letra 'A'
	    $thisNColumns = ord($highestColumn) - 64;

		// Imprimo en pantalla datos adicionales
		    $Htmltable.= BR."<b>Excel: ".$worksheetTitle."</b>";
		    if (!isset($layoutConfig['range']) && $thisNColumns != $layoutConfig['totalColsExpected'] || !isset($layoutConfig['range']) && $highestRow != $layoutConfig['totalRowsExpected'] ){ 
		    	$errorsLayout.= BR.lang('no_rango_definido_y_dimension_incompatible'); 
		    }

	    // Compruebo las dimensiones dla configuracion coincidan con este excel y muestro mensajes
	    	if($thisNColumns != $layoutConfig['totalColsExpected']){
		    	// Si no hay rango definido entonces importa que el total de columnas definido sea adecuado y marcamos error
		    	if (!isset($layoutConfig['range'])){ 
	    			$flagErrorLayout = true;	
		  			$errorsLayout.= BR.lang('total_columnas_archivo')."=".$thisNColumns.', ';
	    			($thisNColumns > $layoutConfig['totalColsExpected'])? $errorsLayout.= lang('total_columnas_sobrepasa').":".$layoutConfig['totalColsExpected'] : $errorsLayout.= lang('total_columnas_menor').":".$layoutConfig['totalColsExpected'];
		    	}
	    	}
		   
		    if($highestRow != $layoutConfig['totalRowsExpected']){
		    	// Si no hay rango definido entonces importa que el total de columnas definido sea adecuado y marcamos error
		    	if (!isset($layoutConfig['range'])){ 
		    		$flagErrorLayout = true;	
		 			$errorsLayout.=  BR.lang('total_filas_archivo')."=".$highestRow.', ';
		    		($highestRow > $layoutConfig['totalRowsExpected'])? $errorsLayout.= lang('total_filas_sobrepasa').":".$layoutConfig['totalRowsExpected'] : $errorsLayout.= lang('total_filas_menor').":".$layoutConfig['totalRowsExpected'];
		    	}
		    }

	    // Obtengo el estilo
	   		$thisclass = ( isset($layoutConfig['css_class']) ) ? $layoutConfig['css_class']:'';
		 // Inicio la tabla HTML
	    	$Htmltable.= "<br><table border=1 class='".$thisclass."'><tbody>";
	    // itero por filas
	    for ($row = $lowerRow; $row <= $highestRow; ++ $row) {
	        $Htmltable.= '<tr>';
	        // itero por columnas

	        for ($col = $lowerColumn; $col < $highestColumnIndex; ++ $col) {
	            $cell = $worksheet->getCellByColumnAndRow($col, $row);
	            // Con esto solo obtengo el valor bruto (NO RESUELVE LA FORMULA ejemp: =SUM(A1:A7) )
	            // $thisVal = $cell->getValue();

	            // Obtengo el valor calculado (resuelve la formula de la celda y me trae el resultado)
	            // TODO: verificar el formato de las cantidades (las comas y los decimales)
	            $thisVal = $cell->getCalculatedValue();
	            $thisVal = trim($thisVal);
	           // Si la celda es una string vacio entoces null para no comprobar el tipo de dato o hacer $thisVal == "" en las validaciones
	            if ($thisVal == "") {$thisVal = null; }

	        	// Obtengo la celda en formato alfanumerico A1, B1 ...
	        		$cellNameAlfaNum = chr($col+65).$row;
	    // Verifico que empate con la configuracion
	    	// Tengo cellsExpected y existe la llave en el arreglo para las cellExpected ?
	        	if($layoutConfig['cellsExpected']!=null && array_key_exists($cellNameAlfaNum, $layoutConfig['cellsExpected']))
	        	{
	        	    /// obtengo el tipo de dato a partir del valor obtenido
	        		$thisDataType = PHPExcel_Cell_DataType::dataTypeForValue($thisVal);

	        		// Obtengo el split cellexpected ejemp: string|mivalor con trim para casos: '*| abc' , '* | abc', etc 
	        		$splited = split('[|]',trim($layoutConfig['cellsExpected'][$cellNameAlfaNum]));

	        		// lado izq del split es el tipo de valor: n = numero , s = string etc..
	        		$typeExpected = trim($splited[0]);
	        		// lado derecho del split es el valor
	        		$valExpected = trim($splited[1]);

	        		// Si no se define algo en ambos lados de la regla ejemplos: 'a| ' , ' |123' o incluso ' | ' el lado vacio se toma como comodin * (cualquiera)
	        		if(trim($typeExpected)==""){
	        			$typeExpected = '*';
	        		}
	        		if(trim($valExpected)==""){
	        			$valExpected = '*';
	        		}

	        		// Asterisco * = "tipo comodin" (puede ser cualquier tipo de datos, no importa el tipo de dato)
	        		$typeExpected = ($typeExpected == '*')?  $thisDataType : $typeExpected;
	        		// Asterisco * = "valor comodin" (puede ser cualquier valor, no importa el valor)
	        		$valExpected = ($valExpected == '*')? $thisVal : $valExpected;

    	        // El tipo de dato es diferente y el valor actual es diferente de null ?
	    	        if ($thisDataType != $typeExpected && !is_null($thisVal)) {
	    	        	$flagErrorLayout = true;
	    	        	$errorsLayout.= BR.lang('celda').$cellNameAlfaNum." ".lang('tipo_dato_incorecto')." \"".PHPEH_libType_to_humanReadable($thisDataType)."\" ".lang('se_esperaba')." \"".PHPEH_libType_to_humanReadable($typeExpected)."\"";
	    	        }

	    		// Compruebo el valor esperado, vacio ?
	        		if (is_null($thisVal)) {
	        			$flagErrorLayout = true;
	        			$errorsLayout.= BR.lang('celda').$cellNameAlfaNum." \"".lang('vacia')."\" ";
	        			if($thisVal != ""){
	        				$errorsLayout.= lang('se_esperaba')." \"".$valExpected."\"";
	        			}else{
	        				$errorsLayout.= lang('se_esperaba')." ".lang('cualquiera_pero_no_vacia');
	        			}
	        		}else{
		        		if ( $valExpected != $thisVal ) {
	        			$flagErrorLayout = true;
	          			$errorsLayout.= BR.lang('celda').$cellNameAlfaNum." ".lang('valor_incorrecto')." "." \"".$thisVal."\" ".lang('se_esperaba')." \"".$valExpected."\"";
		        		}
	        		}

	        	} 

	            // Si la celda tiene algun dato 
	            if (!is_null($thisVal)) {
	            	// Verifico el valor esperado para una celda X
	            	// Obtengo el tipo de dato de la celda
		            $thisDataType = PHPExcel_Cell_DataType::dataTypeForValue($thisVal);
	    	        // $Htmltable.= '<td>' . $thisVal . '<br>(Type ' . $thisDataType . ')</td>';
	    	        $Htmltable.= '<td>'.$thisVal.'</td>';
	            }else{
	            	$Htmltable.= '<td>&nbsp;</td>';
	            }
	        }
	        $Htmltable.= '</tr>';
	    }
	    $Htmltable.= '</tbody></table>';
	}// end foreach sheets

	// Inicializo arreglo para la respuesta en json resultado = 0 = no hay errores al leer el layout
	$respuesta = array('resultado' => 0);
	// Mensajes de error
    $respuesta['mensaje'] = '';
    // tabla html del archivo excel
	$respuesta['Htmltable'] = $Htmltable;

// Agrego mensajes en dado caso de estar activados
    if ($layoutConfig['show_messages']) {
    	$respuesta['mensaje'] = $errorsLayout;
    }

    // Existe un error en el layout ?
	if ($flagErrorLayout) {
		// Agrego etiqueta para identificar errores en el layout
		// $Htmltable = "</br><label id='errorsLayout'>".lang('layout_errores')."</label>".$Htmltable;
		// 1 = error en el layout
		$respuesta['resultado'] = 1; 
		$respuesta['mensaje'] = lang('layout_incompatible').$respuesta['mensaje'];

	}
	
	return json_encode($respuesta);
}

/**
 * Funcion para cargar un archivo importado de excel excel5 o 2007 a un objPHPExcel 1.8.0
 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
 * @uses    funciones de la libreria objPHPExcel 1.8.0, file_exists()
 * @uses    PHPExcel_IOFactory::identify()
 * @uses    PHPExcel_IOFactory::createReader($inputFileType)
 * @param   string Ruta relativa para el archivo importado
 * @return  objPHPExcel 1.8.0
 *******Notas: En php.init verificar memory_limit sea suficiente.
 * PHP version 5.2.0 o mayor
 * Extensiones PHP necesarias activadas: 
 *              php_zip  (required if you need PHPExcel to handle .xlsx .ods or .gnumeric files)
 *              php_xml
 *              php_gd2 
 * Es necesario copiar la carpeta classes del proyecto phpmyadmin de la lib PHPExcel ya que la version oficial de la pag presenta comportamientos
 *  no esperados incompatibles con este código (no carga los archivos en varios casos). 
 */ 
function PHPEH_excel_to_objPHPExcel($inputFileName)
{
// Include de la clases para la lib PHPExcel
	include('Classes\PHPExcel\IOFactory.php');

// Compruebo que el archivo exista en el directorio
		if(!file_exists($inputFileName)){
			echo '<script type="text/javascript">
					var texto = "'.lang('el_archivo_no_existe_en_dir').'";
					alert(texto);
				  </script>';
		}

// Preparo el archivo:
	// Identifico el tipo de archivo de excel y su version
	$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
	// Cargo el archivo para leerlo y le paso el tipo de archivo identificado
	$objReader = PHPExcel_IOFactory::createReader($inputFileType);
	$objReader->setReadDataOnly(true);

	// Cargo el archivo 
	$objPHPExcel = $objReader->load($inputFileName);


	// Es posible crear el archivo html directamente con las siguientes 3 lineas
	// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'HTML');
	// $objWriter->setSheetIndex(0);
	// $objWriter->save(str_replace(".xls", ".html", $inputFileName));
	
	return $objPHPExcel;
}

/**
 * Funcion para procesar un layout excel importado y regresar un arreglo listo para insertarlo en la BD
 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
 * @uses    PHPEH_is_sheet_in_read_list() Busca una hoja en la lista de hojas permitidas a leer del excel
 * @uses    PHPEH_excel_to_objPHPExcel() Obtiene un objeto PHPExcel
 * @uses    PHPEH_verify_read_range() Configura y prepara el rango de lectura
 * @param   String $filename nombre del archivo importado ya validado
 * @param   Array $layoutConfig configuracion del layout 
 * @param   Array $insertsMap mapeo del archivo de excel para generar el arreglo que se inserta en la BD
 *
 * Documentacion adicional de $insertsMap:
 * El arreglo $insertsMap sirve para indicarle a la funcion como distribuir los datos obtenidos del excel en el arreglo que sera ingresado a la BD posteriormente  
 *  $insertsMap['<nombre del campo de la tabla>'] = <DataMap>;
 * ejemplo: 
 *           $insertsMap['numero_avance'] = 'nColumna';
 *
 *  Aqui muestro las partes del mapeo de un archivo de excel, La lista de <DataMap>:
 * 		nColumna  = num de columna de izq a derecha 
 * 		nFila = num de fila de arriba a abajo
 *		datoCuadrante = datos que estan por debajo del encabezado y enseguida la primera columna
 * 		encabezadoColumna = datos que estan en la primera fila que son encabezado de los datos (titulo columna)
 * 		encabezadoFila = datos que estan en la primera columna de la izq (titulo de la fila)
 * @return  Array $insertArray arreglo con todos los INSERT VALUES ...
 */
function PHPEH_layoutfile_to_array($filename, $layoutConfig, $insertsMap)
{
	// Convierto el objeto a un arreglo para manejarlo con el codigo copypaste de arreglos asosiativos
	$layoutConfig = (array) $layoutConfig;

	// Obtengo el objeto PHPExcel 
	$objPHPExcel = PHPEH_excel_to_objPHPExcel($filename);
		// Obtengo el total de hojas del libro de excel
	$totalsheets = $objPHPExcel->getAllSheets();

// Itero sobre las hojas de excel
// Con este foreach sobre los iteradores podemos leer varias hojas del excel
	foreach ($totalsheets as $worksheet) {
		// Obtengo el titulo de la hoja
	    $worksheetTitle = $worksheet->getTitle();

	    // La hoja de excel actual existe en la lista de hojas a leer ?
	    if (!is_null($layoutConfig['sheetsToRead']) && !PHPEH_is_sheet_in_read_list($layoutConfig['sheetsToRead'], $worksheetTitle)) {
	    	continue;
	    }
	    
		// Inicializo la primera columna y fila de la cual se empieza a leer el archivo default 0,1 = A1 
		$lowerColumn = 0;
		$lowerRow = 1;
		$highestColumn = 0;
		$highestRow = 0;

		// Verifico si existe un rango de lectura para el archivo y si existe lo prepara
		PHPEH_verify_read_range($layoutConfig, $worksheet, $lowerColumn, $lowerRow, $highestColumn, $highestRow);
	
	    // Convierto el indice (nombre de la columna) a valor numerico 
	    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

	   // Arreglo para el insert
	   $insertArray = array();

   // Defino las llaves a utilizar a partir del mapeo:
	   foreach ($insertsMap as $key => $value) {
		   	if ($insertsMap[$key] == 'datoCuadrante'){
		   		$key_datoCuadrante = $key;
		   	}
		   	if ($insertsMap[$key] == 'nColumna'){
		   		$key_nColumna = $key;
		   	}
	   		if ($insertsMap[$key] == 'nFila'){
	   			$key_nFilas = $key;
		   	}
		   	if ($insertsMap[$key] == 'encabezadoColumna'){
		   		$key_encabezadoColumna = $key;
		   	}
		   	if ($insertsMap[$key] == 'encabezadoFila'){
		   		$key_encabezadoFila = $key;
		   	}

		   	// Verifico si hay phpcode para evaluar en el insertMaps
		   	if(strstr($insertsMap[$key], 'PHPCODE:') != FALSE) { 
	 			@eval("\$insertsMap[\$key] = ".trim(str_replace('PHPCODE:', '', $insertsMap[$key])));
			}  

	   }

	   // Necesito contadores adicionales ya que row o col en la iteracion pueden empezar mayores a 1
		   $rowCounter  = 1;
	       $colCounter = 1;	

    // itero por filas
    for ($row = $lowerRow; $row <= $highestRow; ++ $row, $rowCounter++) {
    	// Obtengo el encabezado de la fila actual
        	$cell = $worksheet->getCellByColumnAndRow(0, $row);
        	$thisencabezadoFila = $cell->getCalculatedValue();
    	// reinicio el colCounter
            $colCounter = 1;
        // itero por columnas
        for ($col = $lowerColumn; $col < $highestColumnIndex; $col++, $colCounter++) {
        	
        	// Obtengo el encabezado de la columna actual
        	$cell = $worksheet->getCellByColumnAndRow($col, 1);
        	$thisencabezadoColumna = $cell->getCalculatedValue();
        	unset($cell);
            $cell = $worksheet->getCellByColumnAndRow($col, $row);
            
            // Obtengo el valor calculado (resuelve la formula de la celda y me trae el resultado)
            // TODO: verificar el formato de las cantidades (las comas y los decimales)
            $thisVal = $cell->getCalculatedValue();
            $thisVal = trim($thisVal);
           
           // Si la celda es un string vacio entoces = null para no comprobar el tipo de dato o hacer $thisVal == "" en las validaciones
            if ($thisVal == "") {$thisVal = null; }

        	// Obtengo la celda en formato alfanumerico A1, B1 ...
        		$cellNameAlfaNum = chr($col+65).$row;

            // Si la celda tiene algun dato
            if (!is_null($thisVal)) {
    	    // MAPING: Estoy en una celdad entro del cuadrante ? (area de datos)
            	if($row >= $lowerRow && ($col + 1) > $lowerColumn){	
					// Defino el arreglo para esta iteracion a partir del arreglo mapeado inicializado
					$thisArray = $insertsMap;
		        	
		        	// verifico y asigno el numero de columna al key correspodiente en el mapeado
		        	if (isset($key_nColumna)) {
		        		$thisArray[$key_nColumna] = $colCounter;
		        	}

		        	// verifico y asigno el numero de fila al key correspodiente en el mapeado
		        	if (isset($key_nFilas)) {
		        		$thisArray[$key_nFilas] = $rowCounter;
		        	}

	        	// verifico y asigno el valor de la celda cuadrante (dato)
		        	if (isset($key_datoCuadrante)) {
		        		$thisArray[$key_datoCuadrante] = $thisVal;
		        	}
	        	// verifico y asigno el valor de encabezado de columna
		        	if (isset($key_encabezadoColumna)) {
		        		$thisArray[$key_encabezadoColumna] = $thisencabezadoColumna;
		        	}
	        	// verifico y asigno el valor de nombre de la fila
		        	if (isset($key_encabezadoFila)) {
		        		$thisArray[$key_encabezadoFila] = $thisencabezadoFila;
		        	}
		            // Agrego el arreglo actual obtenido al array total a regresar
		            array_push($insertArray, $thisArray);
            	}

            }

        }// end iteracion columnas
   	  }// end iteracion filas
	}// end foreach sheets
	
	return $insertArray;
}

/**
 * Crea un array asociativo a partir de un result 
 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
 * @param   String $result resultado de la query
 * @param   String $keyname nombre del campo de la tabla que sera conciderado como llave en el arreglo
 * @param   String $value nombre del campo de la tabla que sera conciderado como valor
 * @return  Array $array asociativo
 */
function PHPEH_result_to_array_assoc($result, $keyName, $valueName)
{
	if ($result->num_rows() > 0)
		{
			foreach ($result->result_array() as $row)
			{
			  $array[$row[$keyName]] = $row[$valueName];
			}
			return $array; 
		}else{
			return null;
		} 
}

?>