/**
 *  Carga modal y trae su vista para la importacion del archivo excel
 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
 */
function modal_importar_excel(configModal)
{
	// Ajax para llenar el contenido o vista del modal dialog
	$.ajax({  
			type: 'POST',
			url: configModal.url_index_import_PHPExcel
		}).done( function(html) {	
				var contenido = html;
			// Abro el modal para importar excel
				abrirModal(configModal.id,configModal.titulo,contenido,configModal.footer_buttons);
			// Desactivo el boton para insertar el layout
				 $("#save_import_button").attr('disabled', true);
		}).fail(function(){
				alert(configModal.msj_error_abrir_modal); 
		});
}


/**
 * Funcion para procesar e insertar el layout cargado
 * @author  4L3J4NDR0 4N4Y4 (energy1011@gmail.com) 2014
 */
function insertar_layout(config){
	// Obtengo el tipo de layout
	if (config.layoutType == null){	
		var layoutType = config.layoutType;
	}else{
		var layoutType = $('#layoutType').val();
	}
	$.ajax({  
		type:'POST',
		url: config.url_insert_imported_file,
		dataType: 'json',
		data: {
				layoutType: layoutType+'_'+config.num_layout,
				tableName: layoutType,
				idindicadores: $('#idindicadores').val()
		}
	}).done( function(json) {
		alert(json.mensaje);
		// Cierro el modal importador
  		 $('#modal-import-excel').modal('hide');
  		 // Redireccionamiento despues de cerrar el modal
  		 if (config.redirect_before_insert) {
  		 	if (config.redirect_before_insert == 'self') {
  		 		location.reload();
  		 	}else{		
  		 		document.location.href = config.redirect_before_insert;
  		 	}
  		 };
	}).fail(function(json){
		alert(json.mensaje);
	});
}