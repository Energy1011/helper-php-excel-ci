$(document).ready(function() {
	
	var p = $("#cuerpo");
	var position = p.position();

	//Funcion para cerrar el popup  
	$("#cerrar").click( function(){
			$('#pop').animate({   height: 'toggle', width: 'toggle'  });
	});
  
  $("#ayuda").click(function (event){ 
			$("#dialog-modal").dialog({
						width: 560,
						height: 670,
						modal: true,
						buttons: {
						Ok: function() {	$(this).dialog("close");	}
						},
					close : function(event, ui)	{ $("#dialog-modal").dialog( "destroy" ); },
					title: " "
					});
				$.ajax({	type: "GET", url: "./ayuda_primera",	
							data: "", 
							success: function(html) { $("#dialog-modal").html(html);	 }	});
	  });

	  $("a.ver").click(function (event){
   
		$("html").mousemove(function(e){	$('.follow').css('top', e.clientY).css('left', e.clientX  - position.left ); 	});
		$('.follow').css('visibility', 'visible'); 
			
			var a  = this;
			
			$("#pop-content").html(' ');
			
			var dataString = "registro="+ a.rel;
			$.ajax({
					type: "POST",
					url: "detalle",
					data: dataString,
					success: function(html) {	

					$("#pop-content").html(html);
					$('.follow').css('visibility', 'hidden');
					
					//Abrir el PopUp
					if ($('#pop').is(':hidden')) 
						$('#pop').animate({   height: 'toggle'  });
					
					}
				});
		//Posicion
		$('#pop').css('top', event.pageY  );
		$('#pop').css('left', event.pageX - position.left  );

		//Darle el alto y ancho 
		var img_w = 500;
		var img_h = 150;
		$("#pop").width(img_w);
		$("#pop").height(img_h);
		$("#pop").css('width', 'auto');  
		$("#pop").css('height', 'auto');  

		return false;
   }); 
});
