-- phpMyAdmin SQL Dump
-- version 4.2.3
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 10-09-2014 a las 17:04:57
-- Versión del servidor: 5.5.35-1ubuntu1
-- Versión de PHP: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `planeacion`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`developer`@`%` PROCEDURE `calcula_niveles`()
BEGIN

  DECLARE done INT DEFAULT FALSE;
  DECLARE tidobjetivos INT;
  DECLARE tid_padre INT;
  DECLARE tnivel varchar(45);
  DECLARE cur1 CURSOR FOR SELECT idobjetivos,id_padre,nivel FROM planeacion.cat_objetivos WHERE id_padre != 0;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  -- Setea todos los registros a -1
  UPDATE cat_objetivos SET nivel = null;
  -- Setea a todos los padres en nivel 1
  UPDATE planeacion.cat_objetivos SET nivel = 1 WHERE id_padre  = 0;

OPEN cur1;

  read_loop: LOOP
    FETCH cur1 INTO tidobjetivos, tid_padre, tnivel;
  SET @pid_nivel = -1011;
    IF done THEN
      LEAVE read_loop;
    END IF;

 IF tid_padre != 0 THEN
   SELECT tnivel; 
    -- obtengo su registro padre
    SELECT nivel INTO @pid_nivel FROM planeacion.cat_objetivos WHERE idobjetivos=tid_padre;
    -- actualizo su nivel
     UPDATE cat_objetivos SET nivel = @pid_nivel + 1 WHERE idobjetivos=tidobjetivos;
   END IF;
  END LOOP;

CLOSE cur1;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `phpexcel_cellsexpected`
--

CREATE TABLE IF NOT EXISTS `phpexcel_cellsexpected` (
  `idLayout` int(11) NOT NULL,
  `cell` varchar(25) CHARACTER SET utf8 NOT NULL,
  `rule` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `phpexcel_cellsexpected`
--
-- Ejemplo de registros
-- INSERT INTO `phpexcel_cellsexpected` (`idLayout`, `cell`, `rule`) VALUES
-- (1, 'A1', 's|Codigo'),
-- (1, 'A10', 'n|*'),
-- (1, 'A11', 'n|*'),
-- (1, 'A12', 'n|*'),
-- (1, 'A13', 'n|*'),
-- (1, 'A14', 'n|*'),
-- (1, 'A15', 'n|*'),
-- (1, 'A16', 'n|*'),
-- (1, 'A17', 'n|*'),
-- (1, 'A18', 'n|*'),
-- (1, 'A19', 'n|*'),
-- (1, 'A2', 'n|*'),
-- (1, 'A20', 'n|*'),
-- (1, 'A21', 'n|*'),
-- (1, 'A22', 'n|*'),
-- (1, 'A23', 'n|*'),
-- (1, 'A24', 'n|*'),
-- (1, 'A25', 'n|*'),
-- (1, 'A26', 'n|*'),
-- (1, 'A27', 'n|*'),
-- (1, 'A28', 'n|*'),
-- (1, 'A29', 'n|*'),
-- (1, 'A3', 'n|*'),
-- (1, 'A30', 'n|*'),
-- (1, 'A31', 'n|*'),
-- (1, 'A32', 'n|*'),
-- (1, 'A33', 'n|*'),
-- (1, 'A4', 'n|*'),
-- (1, 'A5', 'n|*'),
-- (1, 'A6', 'n|*'),
-- (1, 'A7', 'n|*'),
-- (1, 'A8', 'n|*'),
-- (1, 'A9', 'n|*'),
-- (1, 'B1', 's|Entidad Federativa'),
-- (1, 'C1', 's|Enero'),
-- (1, 'D1', 's|Febrero'),
-- (1, 'E1', 's|Marzo'),
-- (1, 'F1', 's|Abril'),
-- (1, 'G1', 's|Mayo'),
-- (1, 'H1', 's|Junio'),
-- (1, 'I1', 's|Julio'),
-- (1, 'J1', 's|Agosto'),
-- (1, 'K1', 's|Septiembre'),
-- (1, 'L1', 's|Octubre'),
-- (1, 'M1', 's|Noviembre'),
-- (1, 'N1', 's|Diciembre'),
-- (3, 'A1', 's|Codigo'),
-- (3, 'A10', 'n|*'),
-- (3, 'A11', 'n|*'),
-- (3, 'A12', 'n|*'),
-- (3, 'A13', 'n|*'),
-- (3, 'A14', 'n|*'),
-- (3, 'A15', 'n|*'),
-- (3, 'A16', 'n|*'),
-- (3, 'A17', 'n|*'),
-- (3, 'A18', 'n|*'),
-- (3, 'A19', 'n|*'),
-- (3, 'A2', 'n|*'),
-- (3, 'A20', 'n|*'),
-- (3, 'A21', 'n|*'),
-- (3, 'A22', 'n|*'),
-- (3, 'A23', 'n|*'),
-- (3, 'A24', 'n|*'),
-- (3, 'A25', 'n|*'),
-- (3, 'A26', 'n|*'),
-- (3, 'A27', 'n|*'),
-- (3, 'A28', 'n|*'),
-- (3, 'A29', 'n|*'),
-- (3, 'A3', 'n|*'),
-- (3, 'A30', 'n|*'),
-- (3, 'A31', 'n|*'),
-- (3, 'A32', 'n|*'),
-- (3, 'A33', 'n|*'),
-- (3, 'A4', 'n|*'),
-- (3, 'A5', 'n|*'),
-- (3, 'A6', 'n|*'),
-- (3, 'A7', 'n|*'),
-- (3, 'A8', 'n|*'),
-- (3, 'A9', 'n|*'),
-- (3, 'B1', 's|Entidad Federativa'),
-- (3, 'C1', 's|Enero-Diciembre'),
-- (4, 'A1', 's|Codigo'),
-- (4, 'A10', 'n|*'),
-- (4, 'A11', 'n|*'),
-- (4, 'A12', 'n|*'),
-- (4, 'A13', 'n|*'),
-- (4, 'A14', 'n|*'),
-- (4, 'A15', 'n|*'),
-- (4, 'A16', 'n|*'),
-- (4, 'A17', 'n|*'),
-- (4, 'A18', 'n|*'),
-- (4, 'A19', 'n|*'),
-- (4, 'A2', 'n|*'),
-- (4, 'A20', 'n|*'),
-- (4, 'A21', 'n|*'),
-- (4, 'A22', 'n|*'),
-- (4, 'A23', 'n|*'),
-- (4, 'A24', 'n|*'),
-- (4, 'A25', 'n|*'),
-- (4, 'A26', 'n|*'),
-- (4, 'A27', 'n|*'),
-- (4, 'A28', 'n|*'),
-- (4, 'A29', 'n|*'),
-- (4, 'A3', 'n|*'),
-- (4, 'A30', 'n|*'),
-- (4, 'A31', 'n|*'),
-- (4, 'A32', 'n|*'),
-- (4, 'A33', 'n|*'),
-- (4, 'A4', 'n|*'),
-- (4, 'A5', 'n|*'),
-- (4, 'A6', 'n|*'),
-- (4, 'A7', 'n|*'),
-- (4, 'A8', 'n|*'),
-- (4, 'A9', 'n|*'),
-- (4, 'B1', 's|Entidad Federativa'),
-- (4, 'C1', 's|Enero-Junio'),
-- (4, 'D1', 's|Julio-Diciembre'),
-- (5, 'A1', 's|Codigo'),
-- (5, 'A10', 'n|*'),
-- (5, 'A11', 'n|*'),
-- (5, 'A12', 'n|*'),
-- (5, 'A13', 'n|*'),
-- (5, 'A14', 'n|*'),
-- (5, 'A15', 'n|*'),
-- (5, 'A16', 'n|*'),
-- (5, 'A17', 'n|*'),
-- (5, 'A18', 'n|*'),
-- (5, 'A19', 'n|*'),
-- (5, 'A2', 'n|*'),
-- (5, 'A20', 'n|*'),
-- (5, 'A21', 'n|*'),
-- (5, 'A22', 'n|*'),
-- (5, 'A23', 'n|*'),
-- (5, 'A24', 'n|*'),
-- (5, 'A25', 'n|*'),
-- (5, 'A26', 'n|*'),
-- (5, 'A27', 'n|*'),
-- (5, 'A28', 'n|*'),
-- (5, 'A29', 'n|*'),
-- (5, 'A3', 'n|*'),
-- (5, 'A30', 'n|*'),
-- (5, 'A31', 'n|*'),
-- (5, 'A32', 'n|*'),
-- (5, 'A33', 'n|*'),
-- (5, 'A4', 'n|*'),
-- (5, 'A5', 'n|*'),
-- (5, 'A6', 'n|*'),
-- (5, 'A7', 'n|*'),
-- (5, 'A8', 'n|*'),
-- (5, 'A9', 'n|*'),
-- (5, 'B1', 's|Entidad Federativa'),
-- (5, 'C1', 's|Enero-Abril'),
-- (5, 'A1', 's|Codigo'),
-- (5, 'A10', 'n|*'),
-- (5, 'A11', 'n|*'),
-- (5, 'A12', 'n|*'),
-- (5, 'A13', 'n|*'),
-- (5, 'A14', 'n|*'),
-- (5, 'A15', 'n|*'),
-- (5, 'A16', 'n|*'),
-- (5, 'A17', 'n|*'),
-- (5, 'A18', 'n|*'),
-- (5, 'A19', 'n|*'),
-- (5, 'A2', 'n|*'),
-- (5, 'A20', 'n|*'),
-- (5, 'A21', 'n|*'),
-- (5, 'A22', 'n|*'),
-- (5, 'A23', 'n|*'),
-- (5, 'A24', 'n|*'),
-- (5, 'A25', 'n|*'),
-- (5, 'A26', 'n|*'),
-- (5, 'A27', 'n|*'),
-- (5, 'A28', 'n|*'),
-- (5, 'A29', 'n|*'),
-- (5, 'A3', 'n|*'),
-- (5, 'A30', 'n|*'),
-- (5, 'A31', 'n|*'),
-- (5, 'A32', 'n|*'),
-- (5, 'A33', 'n|*'),
-- (5, 'A4', 'n|*'),
-- (5, 'A5', 'n|*'),
-- (5, 'A6', 'n|*'),
-- (5, 'A7', 'n|*'),
-- (5, 'A8', 'n|*'),
-- (5, 'A9', 'n|*'),
-- (5, 'B1', 's|Entidad Federativa'),
-- (5, 'C1', 's|Enero-Abril'),
-- (5, 'D1', 's|Mayo-Agosto'),
-- (5, 'E1', 's|Septiembre-Diciembre'),
-- (6, 'A1', 's|Codigo'),
-- (6, 'A10', 'n|*'),
-- (6, 'A11', 'n|*'),
-- (6, 'A12', 'n|*'),
-- (6, 'A13', 'n|*'),
-- (6, 'A14', 'n|*'),
-- (6, 'A15', 'n|*'),
-- (6, 'A16', 'n|*'),
-- (6, 'A17', 'n|*'),
-- (6, 'A18', 'n|*'),
-- (6, 'A19', 'n|*'),
-- (6, 'A2', 'n|*'),
-- (6, 'A20', 'n|*'),
-- (6, 'A21', 'n|*'),
-- (6, 'A22', 'n|*'),
-- (6, 'A23', 'n|*'),
-- (6, 'A24', 'n|*'),
-- (6, 'A25', 'n|*'),
-- (6, 'A26', 'n|*'),
-- (6, 'A27', 'n|*'),
-- (6, 'A28', 'n|*'),
-- (6, 'A29', 'n|*'),
-- (6, 'A3', 'n|*'),
-- (6, 'A30', 'n|*'),
-- (6, 'A31', 'n|*'),
-- (6, 'A32', 'n|*'),
-- (6, 'A33', 'n|*'),
-- (6, 'A4', 'n|*'),
-- (6, 'A5', 'n|*'),
-- (6, 'A6', 'n|*'),
-- (6, 'A7', 'n|*'),
-- (6, 'A8', 'n|*'),
-- (6, 'A9', 'n|*'),
-- (6, 'B1', 's|Entidad Federativa'),
-- (6, 'C1', 's|Enero-Marzo'),
-- (6, 'D1', 's|Abril-Junio'),
-- (6, 'E1', 's|Julio-Agosto'),
-- (6, 'F1', 's|Septiembre-Diciembre'),
-- (7, 'A1', 's|Codigo'),
-- (7, 'A10', 'n|*'),
-- (7, 'A11', 'n|*'),
-- (7, 'A12', 'n|*'),
-- (7, 'A13', 'n|*'),
-- (7, 'A14', 'n|*'),
-- (7, 'A15', 'n|*'),
-- (7, 'A16', 'n|*'),
-- (7, 'A17', 'n|*'),
-- (7, 'A18', 'n|*'),
-- (7, 'A19', 'n|*'),
-- (7, 'A2', 'n|*'),
-- (7, 'A20', 'n|*'),
-- (7, 'A21', 'n|*'),
-- (7, 'A22', 'n|*'),
-- (7, 'A23', 'n|*'),
-- (7, 'A24', 'n|*'),
-- (7, 'A25', 'n|*'),
-- (7, 'A26', 'n|*'),
-- (7, 'A27', 'n|*'),
-- (7, 'A28', 'n|*'),
-- (7, 'A29', 'n|*'),
-- (7, 'A3', 'n|*'),
-- (7, 'A30', 'n|*'),
-- (7, 'A31', 'n|*'),
-- (7, 'A32', 'n|*'),
-- (7, 'A33', 'n|*'),
-- (7, 'A4', 'n|*'),
-- (7, 'A5', 'n|*'),
-- (7, 'A6', 'n|*'),
-- (7, 'A7', 'n|*'),
-- (7, 'A8', 'n|*'),
-- (7, 'A9', 'n|*'),
-- (7, 'B1', 's|Entidad Federativa'),
-- (7, 'C1', 's|Enero-Febrero'),
-- (7, 'D1', 's|Marzo-Abril'),
-- (7, 'E1', 's|Mayo-Junio'),
-- (7, 'F1', 's|Julio-Agosto'),
-- (7, 'G1', 's|Septiembre-Octubre'),
-- (7, 'H1', 's|Noviembre-Diciembre'),
-- (2, 'A2', 's|Codigo'),
-- (2, 'A10', 'n|*'),
-- (2, 'A11', 'n|*'),
-- (2, 'A12', 'n|*'),
-- (2, 'A13', 'n|*'),
-- (2, 'A14', 'n|*'),
-- (2, 'A15', 'n|*'),
-- (2, 'A16', 'n|*'),
-- (2, 'A17', 'n|*'),
-- (2, 'A18', 'n|*'),
-- (2, 'A19', 'n|*'),
-- (2, 'A2', 's|*'),
-- (2, 'A20', 'n|*'),
-- (2, 'A21', 'n|*'),
-- (2, 'A22', 'n|*'),
-- (2, 'A23', 'n|*'),
-- (2, 'A24', 'n|*'),
-- (2, 'A25', 'n|*'),
-- (2, 'A26', 'n|*'),
-- (2, 'A27', 'n|*'),
-- (2, 'A28', 'n|*'),
-- (2, 'A29', 'n|*'),
-- (2, 'A3', 'n|*'),
-- (2, 'A30', 'n|*'),
-- (2, 'A31', 'n|*'),
-- (2, 'A32', 'n|*'),
-- (2, 'A33', 'n|*'),
-- (2, 'A34', 'n|*'),
-- (2, 'A4', 'n|*'),
-- (2, 'A5', 'n|*'),
-- (2, 'A6', 'n|*'),
-- (2, 'A7', 'n|*'),
-- (2, 'A8', 'n|*'),
-- (2, 'A9', 'n|*'),
-- (2, 'B1', 's|Codigo origen ->'),
-- (2, 'B2', 's|Entidad Federativa'),
-- (2, 'C1', 'n|*');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `phpexcel_insertsmap`
--

CREATE TABLE IF NOT EXISTS `phpexcel_insertsmap` (
  `idLayout` int(11) NOT NULL,
  `key` varchar(25) CHARACTER SET utf8 NOT NULL,
  `dataMap` varchar(80) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `phpexcel_insertsmap`
--
--Ejemplo de registros:
-- INSERT INTO `phpexcel_insertsmap` (`idLayout`, `key`, `dataMap`) VALUES
-- (1, 'avance', '-1'),
-- (1, 'idcat_entidad_medible', 'encabezadoFila'),
-- (1, 'meta', 'datoCuadrante'),
-- (1, 'numero_avance', 'nColumna'),
-- (2, 'avance', 'datoCuadrante'),
-- (2, 'idcat_entidad_medible', 'encabezadoFila'),
-- (2, 'numero_avance', 'PHPCODE: date(''n'');'),
-- (2, 'idcat_origenes', 'encabezadoColumna'),
-- (2, 'actualizacion', 'PHPCODE: date(''Y-m-d H:i:s'');'),
-- (1, 'actualizacion', 'PHPCODE: date(''Y-m-d H:i:s'');'),
-- (1, 'actualizado', 'Energy'),
-- (2, 'actualizado', 'Energy'),
-- (3, 'avance', '-1'),
-- (3, 'idcat_entidad_medible', 'encabezadoFila'),
-- (3, 'meta', 'datoCuadrante'),
-- (3, 'numero_avance', 'nColumna'),
-- (3, 'actualizacion', 'PHPCODE: date(''Y-m-d H:i:s'');'),
-- (3, 'actualizado', 'Energy'),
-- (3, 'avance', '-1'),
-- (4, 'idcat_entidad_medible', 'encabezadoFila'),
-- (4, 'meta', 'datoCuadrante'),
-- (4, 'numero_avance', 'nColumna'),
-- (4, 'actualizacion', 'PHPCODE: date(''Y-m-d H:i:s'');'),
-- (4, 'actualizado', 'Energy'),
-- (4, 'avance', '-1'),
-- (5, 'idcat_entidad_medible', 'encabezadoFila'),
-- (5, 'meta', 'datoCuadrante'),
-- (5, 'numero_avance', 'nColumna'),
-- (5, 'actualizacion', 'PHPCODE: date(''Y-m-d H:i:s'');'),
-- (5, 'actualizado', 'Energy'),
-- (6, 'avance', '-1'),
-- (6, 'idcat_entidad_medible', 'encabezadoFila'),
-- (6, 'meta', 'datoCuadrante'),
-- (6, 'numero_avance', 'nColumna'),
-- (6, 'actualizacion', 'PHPCODE: date(''Y-m-d H:i:s'');'),
-- (6, 'actualizado', 'Energy'),
-- (7, 'avance', '-1'),
-- (7, 'idcat_entidad_medible', 'encabezadoFila'),
-- (7, 'meta', 'datoCuadrante'),
-- (7, 'numero_avance', 'nColumna'),
-- (7, 'actualizacion', 'PHPCODE: date(''Y-m-d H:i:s'');'),
-- (7, 'actualizado', 'Energy');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `phpexcel_layouts`
--

CREATE TABLE IF NOT EXISTS `phpexcel_layouts` (
`idLayout` int(11) NOT NULL,
  `layoutType` varchar(50) CHARACTER SET utf8 NOT NULL,
  `totalColsExpected` smallint(6) DEFAULT NULL,
  `totalRowsExpected` smallint(6) DEFAULT NULL,
  `sheetsToRead` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `range` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `kColumns` varchar(150) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `phpexcel_layouts`
--
-- EJEMPLO:
-- INSERT INTO `phpexcel_layouts` (`idLayout`, `layoutType`, `totalColsExpected`, `totalRowsExpected`, `sheetsToRead`, `range`, `kColumns`) VALUES
-- (1, 'indicadores_metas_12', 12, 32, 'Hoja1', 'C2:N*', 'idindicadores,idcat_entidad_medible,numero_avance'),
-- (2, 'indicadores_avances_0', 12, 32, 'Hoja1', 'C3:G34', 'idindicadores,numero_avance,idcat_entidad_medible,idcat_origenes'),
-- (3, 'indicadores_metas_1', 1, 32, 'Hoja1', NULL, 'idindicadores,numero_avance,idcat_entidad_medible'),
-- (4, 'indicadores_metas_2', 2, 32, 'Hoja1', 'C2:D33', 'idindicadores,numero_avance,idcat_entidad_medible'),
-- (5, 'indicadores_metas_3', 3, 32, 'Hoja1', 'C2:E33', 'idindicadores,numero_avance,idcat_entidad_medible'),
-- (6, 'indicadores_metas_4', 4, 32, 'Hoja1', 'C2:F33', 'idindicadores,numero_avance,idcat_entidad_medible'),
-- (7, 'indicadores_metas_6', 6, 32, 'Hoja1', 'C2:H33', 'idindicadores,numero_avance,idcat_entidad_medible');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `phpexcel_cellsexpected`
--
ALTER TABLE `phpexcel_cellsexpected`
 ADD KEY `idLayout` (`idLayout`);

--
-- Indices de la tabla `phpexcel_insertsmap`
--
ALTER TABLE `phpexcel_insertsmap`
 ADD KEY `idLayout` (`idLayout`);

--
-- Indices de la tabla `phpexcel_layouts`
--
ALTER TABLE `phpexcel_layouts`
 ADD PRIMARY KEY (`idLayout`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `phpexcel_layouts`
--
ALTER TABLE `phpexcel_layouts`
MODIFY `idLayout` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
